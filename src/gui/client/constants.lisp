(in-package :constants)

(define-constant +minimum-padding+       2 :test #'=)

(define-constant +ps-file-dialog-filter+ '(("PostScript Files" "*.ps")) :test #'equalp)

(define-constant +stream-status-streaming+   :streaming   :test #'eq)

(define-constant +stream-status-canceled+    :canceled    :test #'eq)

(define-constant +stream-status-downloading+ :downloading :test #'eq)
