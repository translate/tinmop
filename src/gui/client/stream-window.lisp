(in-package :client-stream-window)

(named-readtables:in-readtable nodgui.syntax:nodgui-syntax)

(defclass stream-frame (gui-goodies:table-frame) ())

(defun resync-rows (stream-frame new-rows)
  (with-accessors ((tree gui-goodies:tree)
                   (rows gui-goodies:rows)) stream-frame
    (gui:treeview-delete-all tree)
    (setf rows new-rows)
    (loop for row in rows do
      (let* ((iri                   (getf row :download-iri))
             (stream-client-wrapper (client-main-window:find-db-stream-url iri))
             (stream-status         (to-s (client-main-window::status stream-client-wrapper)))
             (tree-row              (make-instance 'gui:tree-item
                                                   :id    iri
                                                   :text  iri
                                                   :column-values
                                                   (list stream-status
                                                         (to-s (getf row :octect-count)))
                                                   :index gui:+treeview-last-index+)))
        (gui:treeview-insert-item tree :item tree-row)))
    (gui:treeview-refit-columns-width (gui-goodies:tree stream-frame))
  stream-frame))

(defun all-rows ()
  (cev:enqueue-request-and-wait-results :gemini-all-stream-info
                                        1
                                        ev:+standard-event-priority+))

(defmethod initialize-instance :after ((object stream-frame) &key &allow-other-keys)
  (with-accessors ((tree gui-goodies:tree)
                   (rows gui-goodies:rows)) object
    (let ((new-rows (all-rows))
          (treeview (make-instance 'gui:scrolled-treeview
                                    :master  object
                                    :pack    '(:side :top :expand t :fill :both)
                                    :columns (list (_ "Status")
                                                   (_ "Number of octects downloaded")))))
      (setf tree treeview)
      (gui:treeview-heading tree  gui:+treeview-first-column-id+
                            :text (_ "Address"))
      (resync-rows object new-rows)
      object)))

(defun delete-stream-clsr (stream-frame)
  (lambda ()
    (a:when-let* ((selections (gui:treeview-get-selection (gui-goodies:tree stream-frame))))
      (loop for selection in selections do
        (let* ((url                   (gui:id selection))
               (stream-client-wrapper (client-main-window::find-db-stream-url url)))
          (when (eq (client-main-window:status stream-client-wrapper)
                    client-main-window:+stream-status-streaming+)
            (ev:with-enqueued-process-and-unblock ()
              (client-main-window:stop-steaming-stream-thread)))
          (ev:with-enqueued-process-and-unblock ()
            (client-main-window:remove-db-stream stream-client-wrapper)
            (comm:make-request :gemini-remove-stream 1 url))
          (let ((new-rows (all-rows)))
            (resync-rows stream-frame new-rows)))))))

(defun revive-stream-clsr (stream-frame)
  (lambda ()
    (a:when-let* ((selections (gui:treeview-get-selection (gui-goodies:tree stream-frame)))
                  (selection  (first selections)))
      (let* ((url      (gui:id selection))
             (new-rows (all-rows)))
        (client-main-window::open-iri url gui-goodies:*main-frame* t)
        (resync-rows stream-frame new-rows)))))

(defun init-window (master)
  (gui:with-toplevel (toplevel :master master :title (_ "Streams"))
    (gui:transient toplevel master)
    (let* ((table         (make-instance 'stream-frame :master toplevel))
           (buttons-frame (make-instance 'gui:frame :master toplevel))
           (delete-button (make-instance 'gui:button
                                         :master  buttons-frame
                                         :image   icons:*document-delete*
                                         :command (delete-stream-clsr table)))
           (revive-button (make-instance 'gui:button
                                         :master  buttons-frame
                                         :image   icons:*document-accept*
                                         :command (revive-stream-clsr table))))
      (gui-goodies:attach-tooltips (delete-button (_ "delete selected stream"))
                                   (revive-button (_ "show selected stream")))
      (gui:grid table         0 0 :sticky :nwe)
      (gui:grid buttons-frame 1 0 :sticky :s)
      (gui:grid delete-button 0 0 :sticky :s)
      (gui:grid revive-button 0 1 :sticky :s))))
