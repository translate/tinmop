(in-package :client-main-window)

(defun make-internal-iri (path &optional (query nil) (fragment nil))
  (iri:make-iri +internal-scheme+ nil nil nil path query fragment))

(defun internal-iri-bookmark ()
  (make-internal-iri +internal-path-bookmark+))

(defun internal-iri-gemlogs ()
  (make-internal-iri +internal-path-gemlogs+))

(defun show-bookmarks-page (main-window)
  (ev:with-enqueued-process-and-unblock ()
    (let ((parsed-page (comm:make-request :gemini-generate-bookmark-page 1))
          (iri         (internal-iri-bookmark)))
      (set-address-bar-text main-window (to-s iri))
      (clear-gemtext main-window)
      (client-main-window::initialize-ir-lines main-window)
      (gui:focus (toc-frame main-window))
      (collect-ir-lines (to-s iri) main-window parsed-page))))
