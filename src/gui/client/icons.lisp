(in-package :icons)

(a:define-constant +icon-dir+         "/data/icons/"               :test #'string=)

(a:define-constant +search+             "fmw_search"               :test #'string=)

(a:define-constant +back+               "fmw_back"                 :test #'string=)

(a:define-constant +go+                 "fmw_go"                   :test #'string=)

(a:define-constant +open-tour+          "fmw_open_tour"            :test #'string=)

(a:define-constant +refresh+            "fmw_refresh"              :test #'string=)

(a:define-constant +up+                 "fmw_uparrow"              :test #'string=)

(a:define-constant +document-delete+    "fmw_document-delete"      :test #'string=)

(a:define-constant +document-add+       "fmw_document-add"         :test #'string=)

(a:define-constant +document-accept+    "fmw_document-accept"      :test #'string=)

(a:define-constant +document-edit+      "fmw_document-edit"        :test #'string=)

(a:define-constant +folder+             "fmw_folder"               :test #'string=)

(a:define-constant +star-yellow+        "fmw_star-yellow.png"      :test #'string=)

(a:define-constant +star-blue+          "fmw_star-blue.png"        :test #'string=)

(a:define-constant +arrow-up+           "fmw_arrow-up"             :test #'string=)

(a:define-constant +arrow-down+         "fmw_arrow-down"           :test #'string=)

(a:define-constant +cross+              "fmw_cross"                :test #'string=)

(a:define-constant +bus-go+             "fmw_bus-go"               :test #'string=)

(a:define-constant +dice+               "fmw_dice"                 :test #'string=)

(a:define-constant +gemlog-subscribe+   "fmw_rss-add.png"          :test #'string=)

(a:define-constant +gemlog-unsubscribe+ "fmw_rss-delete.png"       :test #'string=)

(a:define-constant +inline-images+      "fmw_two-pictures.png"     :test #'string=)

(a:define-constant +text+               "fmw_text.png"             :test #'string=)

(defparameter *search*             nil)

(defparameter *back*               nil)

(defparameter *open-iri*           nil)

(defparameter *open-tour*          nil)

(defparameter *refresh*            nil)

(defparameter *up*                 nil)

(defparameter *document-delete*    nil)

(defparameter *document-add*       nil)

(defparameter *document-accept*    nil)

(defparameter *document-edit*      nil)

(defparameter *folder*             nil)

(defparameter *star-yellow*        nil)

(defparameter *star-blue*          nil)

(defparameter *arrow-up*           nil)

(defparameter *arrow-down*         nil)

(defparameter *cross*              nil)

(defparameter *bus-go*             nil)

(defparameter *dice*               nil)

(defparameter *gemlog-subscribe*   nil)

(defparameter *gemlog-unsubscribe* nil)

(defparameter *inline-images*      nil)

(defparameter *text*               nil)

(defun load-icon (filename)
  (let ((path (if (not (re:scan "(?i)png$" filename))
                  (res:get-data-file (fs:cat-parent-dir +icon-dir+
                                                          (strcat filename ".png")))
                  (res:get-data-file (fs:cat-parent-dir +icon-dir+ filename)))))
    (with-open-file (stream path :element-type '(unsigned-byte 8))
      (let ((data (gui-utils:read-into-array stream (file-length stream))))
        (gui:make-image data)))))

(defun load-icons ()
  (setf *search*             (load-icon +search+))
  (setf *back*               (load-icon +back+))
  (setf *open-iri*           (load-icon +go+))
  (setf *open-tour*          (load-icon +open-tour+))
  (setf *refresh*            (load-icon +refresh+))
  (setf *up*                 (load-icon +up+))
  (setf *document-delete*    (load-icon +document-delete+))
  (setf *document-add*       (load-icon +document-add+))
  (setf *document-accept*    (load-icon +document-accept+))
  (setf *document-edit*      (load-icon +document-edit+))
  (setf *folder*             (load-icon +folder+))
  (setf *star-yellow*        (load-icon +star-yellow+))
  (setf *star-blue*          (load-icon +star-blue+))
  (setf *arrow-up*           (load-icon +arrow-up+))
  (setf *arrow-down*         (load-icon +arrow-down+))
  (setf *cross*              (load-icon +cross+))
  (setf *bus-go*             (load-icon +bus-go+))
  (setf *dice*               (load-icon +dice+))
  (setf *gemlog-subscribe*   (load-icon +gemlog-subscribe+))
  (setf *gemlog-unsubscribe* (load-icon +gemlog-unsubscribe+))
  (setf *inline-images*      (load-icon +inline-images+))
  (setf *text*               (load-icon +text+)))
