(in-package :validation)

(defgeneric validate (object datum))

(defclass validator ()
  ((error-message
   :initform ""
   :initarg :error-message
   :accessor error-message)
   (validation-function
   :initform (lambda (datum) datum)
   :initarg :validation-function
   :accessor validation-function)))

(defmethod validate ((object validator) datum)
  (when (not (funcall (validation-function datum)))
    (error-message object)))

(defclass regexp-validator (validator)
  ((regexp
   :initform ".*"
   :initarg :regexp
   :accessor regexp)
   (error-message
   :initform ".*"
   :initarg :error-message
   :accessor error-message)))

(defmethod regexp-validate ((object regexp-validator) datum)
  (when (not (re:scan (regexp object) datum))
    (error-message object)))
