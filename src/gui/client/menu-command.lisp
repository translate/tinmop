(in-package :client-menu-command)

(defun help-about ()
  (let ((master gui-goodies:*toplevel*))
    (gui:with-toplevel (toplevel :master master :title (_ "About"))
      (gui:transient toplevel master)
      (let* ((font         (gui-goodies:make-font "sans" 12 nil nil nil))
             (link-font    (gui-goodies:make-font "sans" 12 :bold nil t))
             (text-widget  (make-instance 'gui:scrolled-text
                                          :font   font
                                          :master toplevel
                                          :cursor gui:+standard-cursor+
                                          :use-horizontal-scrolling-p nil
                                          :read-only t))
             (button-frame (make-instance 'gui:frame :master toplevel))
             (close-button (make-instance 'gui:button
                                          :image   icons:*cross*
                                          :master  button-frame
                                          :command (lambda ()
                                                     (gui:exit-from-toplevel toplevel)))))
        (setf (gui:text text-widget) (format nil +help-about-message-no-uri+))
        (gui:make-link-button text-widget
                              `(:line 1 :char 0)
                              `(:line 1 :char :end)
                              link-font
                              (gui-goodies:parse-color "blue")
                              (gui-goodies:parse-color "white")
                              (lambda ()
                                (os-utils:xdg-open +package-url+)))
        (gui:append-line text-widget "For bug report please point your browser to:")
        (gui:append-line text-widget "")
        (gui:append-text text-widget +issue-tracker+)
        (let ((last-line (gui:maximum-lines-number text-widget)))
          (gui:make-link-button text-widget
                                `(:line ,last-line :char 0)
                                (gui:make-indices-end)
                                link-font
                                (gui-goodies:parse-color "blue")
                                (gui-goodies:parse-color "white")
                                (lambda ()
                                  (os-utils:xdg-open +issue-tracker+))))
        (gui-goodies:attach-tooltips (close-button (_ "Close")))
        (gui:grid text-widget  0 0 :sticky :news)
        (gui:grid button-frame 1 0 :sticky :s)
        (gui:grid close-button 0 0 :sticky :n)
        (gui:wait-complete-redraw)))))

(defun quit ()
  (gui:exit-nodgui)
  (client-events:stop-events-loop)
  (comm:close-server))

(defun show-certificates ()
  (let ((master gui-goodies:*toplevel*))
    (client-certificates-window:init-window master)))

(defun show-streams ()
  (let ((master gui-goodies:*toplevel*))
    (client-stream-window:init-window master)))

(defun show-bookmarks-clsr (main-window)
  (lambda ()
    (client-main-window:show-bookmarks-page main-window)))

(defun manage-bookmarks-clsr (main-window)
  (lambda ()
    (client-bookmark-window:manage-bookmarks main-window)))

(defun show-search-frame-clsr (main-window)
  (lambda ()
    (gui:grid (client-main-window::search-frame main-window) 2 0 :sticky :news)
    (gui:focus (client-search-frame::entry (client-main-window::search-frame main-window)))))

(defun show-tour ()
  (let ((master gui-goodies:*toplevel*))
    (client-tour-window:init-window master)))

(defun manage-gemlogs ()
  (let ((master      gui-goodies:*toplevel*)
        (main-window gui-goodies:*main-frame*))
    (client-gemlog-window:init-window master main-window)))
