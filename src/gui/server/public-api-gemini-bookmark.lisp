;; tinmop: an humble gemini and pleroma client
;; Copyright (C) 2022  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.
;; If not, see [[http://www.gnu.org/licenses/][http://www.gnu.org/licenses/]].

(in-package :json-rpc-communication)

(defclass iri-complete-response (box) ())

(defun gemini-bookmark-add (iri section description)
  (if (not (gemini-bookmarked-p iri))
      (progn
        (db:bookmark-add db:+bookmark-gemini-type-entry+
                         iri
                         :section     section
                         :description description)
        t)
      nil))

(defun gemini-generate-bookmark-page ()
  (gemini-parse-string (ui::generate-bookmark-page)))

(defun gemini-bookmark-delete (iri)
  (db:bookmark-delete iri))

(defclass gemini-bookmark-table (box) ())

(defmethod yason:encode ((object gemini-bookmark-table) &optional (stream *standard-output*))
  (encode-flat-array-of-plists (unbox object) stream))

(defun gemini-bookmark-table ()
  (make-instance 'gemini-bookmark-table
                 :contents (db:bookmark-all)))

(defun gemini-bookmarked-p (iri)
  (if (db:bookmark-exists-p iri)
      t
      nil))
